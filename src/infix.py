# expression = '(1+((5+4)-2))'
# expression = '(1+((3+4*2)-2)*5)'
expression = '(3+((8-2*3+6)-4))'
# expression = '(1+((2*5+4)-2))'

priority = {
    '*' : 2,
    '/' : 2,
    '+' : 1,
    '-' : 1,
    '(' : 0,
    ')' : 0
}

operands = []
operators = []

def solve(op, x, y):
    if op == '*': return x * y
    if op == '/': return x / y
    if op == '+': return x + y
    if op == '-': return x - y

for s in expression:
    if s in [str(x) for x in range(10)]:
        operands.append(int(s))
    elif s in ['+', '-', '*', '/'] and len(operators) == 0:
        operators.append(s)
    elif s in ['+', '-', '*', '/'] and priority[s] > priority[operators[-1]]:
        operators.append(s)
    elif s == '(':
        operators.append(s)
    elif s == ')':
        # reverse stacks while stackOperator top != '(' before doing this
        while operators[-1] != '(':
            b = operands.pop()
            a = operands.pop()
            operator = operators.pop()
            operands.append(solve(operator, a, b))
        else:
            operators.pop()
    else:
        b = operands.pop()
        a = operands.pop()
        operator = operators.pop()
        operands.append(solve(operator, a, b))
        operators.append(s)

    print('operands:', operands)
    print('operators:', operators)
    print()
