from stack import Stack


def test(string):
    stack_buffer = Stack()
    stack_output = Stack()
    stack_input = Stack()

    stack_output.push(0)

    input_list = string.split(' ')
    input_list.reverse()
    input_list = map(lambda x: int(x), input_list)

    stack_input.push(input_list)

    print stack_input

    while True:
        if stack_input.size() == 0 and stack_buffer.size() == 0:
            return True, stack_output

        if not stack_input.empity() and stack_input.peek() == stack_output.peek() + 1:
            stack_output.push(stack_input.pop())
        elif not stack_buffer.empity() and stack_buffer.peek() == stack_output.peek() + 1:
            stack_output.push(stack_buffer.pop())
        elif not stack_input.empity():
            stack_buffer.push(stack_input.pop())
        else:
            return False, stack_output


test_string = '1 2 10 5 4 3 7 6 8 9'
# test_string = '5 1 2 4 3'

print test(test_string)
