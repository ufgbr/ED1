class Stack:

    def __init__(self):
        self.__stack = []

    def push(self, e):
        if type(e) is list:
            for i in e:
                self.push(i)
        else:
            self.__stack.append(e)

    def pop(self):
        return self.__stack.pop()

    def peek(self):
        return self.__stack[-1]

    def empity(self):
        return True if len(self.__stack) == 0 else False

    def size(self):
        return len(self.__stack)

    def __repr__(self):
        return str(self.__stack)
